var express = require('express');
var router = express.Router();


var doctor_Controller = require('../controllers/doctorController');


router.get('/', doctor_Controller.index);

router.get('/create', doctor_Controller.doctor_create_get);

router.post('/create', doctor_Controller.doctor_create_post);

router.get('/doctors', doctor_Controller.doctor_list);

router.get('/delete/:id', doctor_Controller.doctor_delete_get);

router.get('/update/:id', doctor_Controller.doctor_update_get);

router.post('/update/:id', doctor_Controller.doctor_update_post);

router.get('/:id', doctor_Controller.doctor_detail);

module.exports = router;