var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var diseasesRouter = require('./routes/disease');
var doctorsRouter = require('./routes/doctor');
var patientsRouter = require('./routes/patient');
var patientHistoryRouter = require('./routes/patientHistory');
var proficiencyRouter = require('./routes/proficiency');
var doctorExperienceRouter = require('./routes/doctorProficiency');
var surgeriesRouter = require('./routes/surgeries');
var compression = require('compression');
var helmet = require('helmet');
const cors = require('cors');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(helmet());
app.use(compression()); //Compress all routes
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/disease', diseasesRouter);
app.use('/doctor', doctorsRouter);
app.use('/patient', patientsRouter);
app.use('/patientHistory', patientHistoryRouter);
app.use('/proficiency', proficiencyRouter);
app.use('/doctorExperience', doctorExperienceRouter);
app.use('/surgerie', surgeriesRouter);

//Set up mongoose connection
var mongoose = require('mongoose');
var mongoDB = process.env.MONGODB_URI ||'mongodb://masroor:12345@ds151433.mlab.com:51433/hospital';
// var mongoDB = 'mongodb://localhost:27017/hospital';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
