var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var ProficiencySchema = new Schema(
    {
    title : {type: String,  required: [true, 'Title is required !']},  
    }
  );

ProficiencySchema
.virtual('url')
.get(function () {
  return '/proficiency/'+this._id;
});

module.exports = mongoose.model('Proficiency', ProficiencySchema);
