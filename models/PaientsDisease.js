var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var PaientsDiseaseSchema = new Schema(
    {
      patient: { type: Schema.ObjectId, ref: 'Patient', required: true },
      disease: { type: Schema.ObjectId, ref: 'Disease', required: true }
    }
  );

// Export model.
module.exports = mongoose.model('PaientsDisease', PaientsDiseaseSchema);
