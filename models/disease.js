var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DiseaseSchema = new Schema({
    title: {type: String,  required: [true, 'Title is required !']},
    disc: {type: String},    
    });

  DiseaseSchema
.virtual('url')
.get(function () {
  return '/disease/'+this._id;
});

// Export model.
module.exports = mongoose.model('Disease', DiseaseSchema);
