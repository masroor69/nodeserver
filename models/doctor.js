var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var DoctorsSchema = new Schema(
    {
    title : {type: String,  required: [true, 'Title is required !']  , enum: ['Mr', 'Mrs', 'Miss',]},  
    name: {type: String, required: [true, 'Name is required !'], max: 100},
    age: {type: Number, required: [true, 'Age is required !']},
    }
  );

// Virtual for author "full" name.
DoctorsSchema
.virtual('fullName')
.get(function () {
  return this.title +', '+this.name;
});
DoctorsSchema
.virtual('url')
.get(function () {
  return '/doctor/'+this._id;
});



// Export model.
module.exports = mongoose.model('Doctor', DoctorsSchema);
