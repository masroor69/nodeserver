var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var PatientSchema = new Schema(
    {
    title : {type: String,  required: [true, 'Title is required !'] , enum: ['Mr', 'Mrs', 'Miss',]},  
    name: {type: String, required: [true, 'Name is required !'], max: 100},
    age: {type: Number, required: [true, 'Age is required !']},
    smoking: Boolean ,
    }
  );

// Virtual for author "full" name.
PatientSchema
.virtual('fullName')
.get(function () {
  return this.title +', '+this.name;
});


PatientSchema
.virtual('url')
.get(function () {
  return '/patient/'+this._id;
});


// Export model.
module.exports = mongoose.model('Patient', PatientSchema);
