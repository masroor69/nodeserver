var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var DoctorsProficiencySchema = new Schema(
    {
      doctor: { type: Schema.ObjectId, ref: 'Doctor', required: true },
      proficiency: { type: Schema.ObjectId, ref: 'Proficiency', required: true }
    }
  );

// Export model.
module.exports = mongoose.model('DoctorsProficiency', DoctorsProficiencySchema);
