var Doctor = require('../models/doctor');
var Patient = require('../models/patient');
var Surgerie = require('../models/surgerie');



const { check,body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
const moment = require('moment');
var async = require('async');


exports.index = function(req, res) { 
    res.send("The Surgeries Index .");
};

exports.surgeries_list = function (req, res) {
        Surgerie
        .find()
        .exec(function (err, surgeries) {
            res.json( {
                surgeries: surgeries,
            })
        })
};

exports.surgeries_list_post = function (req, res) {
        var queryString = ""; 
        let query = Surgerie
        .find();
        if(req.body.active == true){
            query.where({'active': req.body.active });
            queryString += '- Active  '
        }
        if(req.body.active !==""){
            if(req.body.active === false){
                query.where({'active': {$eq:req.body.active} });
                queryString += '- DeActive  '
                
            }
        }
        if(req.body.doctors){
            var DoctorObj = req.body.doctors;
            query.where({'doctor._id': {$in:DoctorObj._id} });
            queryString +='- Doctor =  ' + DoctorObj.title +' '+  DoctorObj.name
            
        }
        if(req.body.startTime){
            query.where({'startDate': {"$gte": req.body.startTime } });
            queryString += '- From >=   ' + moment(req.body.startTime).format('YYYY/MM/DD, HH:mm');
        }
        if(req.body.endTime){
            query.where({'endDate': {"$lt": req.body.endTime } });
            queryString += ' - To <   ' + moment(req.body.endTime).format('YYYY/MM/DD, HH:mm');
        }
        query.exec(function (err, surgeries) {
        Surgerie.count().exec(function (err, count) {
            res.json( {
                surgeries: surgeries
                , queryString : queryString 
            })
        })
        })
                
};

exports.surgeries_detail = function(req, res, next) {
    Surgerie.findById(req.params.id)
    .exec(function (err, surgerie) {
      if (err) { return next(err) }
      if (surgerie==null) { 
          var err = new Error('Surgerie not found');
          err.status = 404;
        }
        res.json({ title: 'Surgerie:', Surgerie: surgerie});
    })
};

exports.surgeries_create_get = function(req, res) {
    async.parallel({
        patients: function(callback){
              Patient.find({})
              .exec(callback);
           },
        doctors: function(callback){
            Doctor.find({})
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                    res.render('./surgeries/surgeries_form', { title: 'Create Surgerie' ,doctors:  results.doctors ,patients: results.patients});
                })
            };
            
exports.surgeries_create_post =[
    body('title', 'Doctor e Invalid between 0 - 99 ').trim(),
    (req, res, next) => {
            errsObj = [];
            let doctorsObj = new Array();
            if(Array.isArray(req.body.doctors)){
                
            req.body.doctors.map((doctor)=>{
                doctorsObj.push( {
                      _id,
                      title,
                      name,  
                  }=  doctor );  
           });
        }else{
            doctorsObj.push( {
                _id,
                title,
                name,  
            }=  req.body.doctors );
        }
        var endDateFormated =   moment(req.body.endTime).format('YYYY/MM/DD, HH:mm');
        var startDateFormated =  moment(req.body.startTime).format('YYYY/MM/DD, HH:mm');
        const errors = validationResult(req);
        async.each(doctorsObj, function(doctor, callback) {
            console.log('Processing Doctor ' + doctor);
            Surgerie.find()
            .where({'doctor._id': doctor._id })
            .where({'startDate': {"$gte": req.body.startTime} })
            .where({'endDate': {"$lte": req.body.endTime} })
            .exec(function (err, found_surgeries) {
                if(found_surgeries.length > 0 ){
                    // console.log('found Doctor');
                    errsObj.push( {msg:`Dr ${doctor.title} ${doctor.name} is Busy That Day ... `} );
                }
                callback();
            });
        }, function(err) {
            if(errsObj.length >0 ){
                return res.json( { errors: errsObj  });
            }else{
                var surgerie = new Surgerie({
                    title : req.body.title,
                    active: req.body.active ,
                    startDate:startDateFormated,
                    endDate:endDateFormated,
                    patient:req.body.patient,
                    doctor: [...doctorsObj],
                    room: req.body.room,
                });
                surgerie.save(function (err) {
                    // console.log('Save Object')
                    if (err) { return next(err); }
                    console.log("Done async Excuting ");
                    res.json( { save: true  });
                });
            }
        });
    }
]

exports.surgeries_delete_get =function(req, res, next) {
    Surgerie.remove({_id:req.params.id},function(err) {
    if (err) { return next(err); }
        res.json( {delete :'done'} );
    });
};

exports.surgeries_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED:  Delete POST');
};

exports.surgeries_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED:  update POST');
};
exports.surgeries_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Update POST');
};
