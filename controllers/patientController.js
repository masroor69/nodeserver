var Patient = require('../models/patient');
var Disease = require('../models/disease');
var PatientDisease = require('../models/PaientsDisease');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.index = function(req, res) { 
    res.render('./patient/index');
};
exports.patient_list = function (req, res) {
      Patient
      .find()
      .exec(function (err, patient) {
            res.json( {
                patients: patient
          })
      })
};


exports.patient_detail = function(req, res) {
    async.parallel({
        historys : function(callback){
              PatientDisease.find({'patient':req.params.id}).populate('disease')
              .exec(callback);
           },
        patient: function(callback){
            Patient.findById(req.params.id)
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                if (results==null) { 
                    var err = new Error('Patient not found');
                    err.status = 404;
                    } 
                res.json( { title: 'Disease:', patient:  results.patient,historys: results.historys});
                })
};

exports.patient_create_get = function(req, res) {
    res.json( { title: 'Create Patient' });
};

exports.patient_create_post =[
    body('title', 'Patient title required').isLength({ min: 1 }).trim(),
    body('name', 'Patient Name required').isLength({ min: 1 }).trim(),
    body('age', 'Patient Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
    sanitizeBody('name').trim().escape(),
    sanitizeBody('age').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        // console.log(req.body)
        var patient = new Patient(
          { title,
            name,
            age,
            smoking
          }= req.body
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json( { title: 'Create Patient', patient: patient, errors: errors.array()});
        return;
        }
        else {
            patient.save(function (err) {
                if (err) { return next(err); }
                res.json( { title: 'Create Patient',save: true , patient: patient, errors: errors.array()});
              });
        }
    }
 ]

exports.patient_delete_get = function(req, res) {
    Patient.remove({_id:req.params.id},function(err) {
        if (err) { return next(err); }
        res.json( {delete :'done'} );
        });
};

exports.patient_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Patient delete POST');
};

exports.patient_update_get = function(req, res) {
    async.parallel({
      historys : function(callback){
            PatientDisease.find({'patient':req.params.id}).populate('disease')
            .exec(callback);
         }, 
    
        disease : function(callback){
            Disease.find({})
            .exec(callback);
         },
        patient : function(callback){
            Patient.findById(req.params.id )
            .exec(callback);
         }
    },
     function (err, results) {
        if(err){return next(err);}
        // console.log(results)
        res.json( { title: 'Update Patient' ,update :'true', patient: results.patient, diseases: results.disease, historys: results.historys,});
    });
};

exports.patient_update_post  =[
     
        body('title', 'Patient title required').isLength({ min: 1 }).trim(),
        body('name', 'Patient Name required').isLength({ min: 1 }).trim(),
        body('age', 'Patient Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var patient = new Patient(
           {title: req.body.title,
            name: req.body.name,
            age: req.body.age,
            smoking: req.body.smoking,
            _id: req.params.id
        }
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.josn( { title: 'Update patient' ,update :'true', patient: patient, errors: errors.array()});
        return;
        }
        else {
            Patient.findByIdAndUpdate(req.params.id, patient, {}, function (err,thepatient) {
                    if (err) { console.log(err) }
                    res.json( {update :'done'} );
                });                        
            }
    }
 ]