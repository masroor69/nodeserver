var Proficiency = require('../models/proficiency');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.index = function(req, res) { 
    res.render('./proficiency/index');
};

exports.proficiency_list = function (req, res) {
      Proficiency
      .find()
      .exec(function (err, proficiency) {
            res.json( {
                proficiencys: proficiency
          })
      })
};

exports.proficiency_detail = function(req, res, next) {
    
    Proficiency.findById(req.params.id)
    .exec(function (err, proficiency) {
      if (err) { return next(err) }
      if (proficiency==null) { 
          var err = new Error('Proficiency not found');
          err.status = 404;
        }
      res.json({ title: 'Proficiency:', proficiency:  proficiency});
    })
};

exports.proficiency_create_get = function(req, res) {
    res.json( { title: 'Create Proficiency' });
};

exports.proficiency_create_post =[
        body('title', 'Proficiency title required').isLength({ min: 1 }).trim(),
        sanitizeBody('title').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var proficiency = new Proficiency(
            {title }=req.body
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json({ title: 'Create Proficiency' , proficiency: proficiency, errors: errors.array()});
        return;
        }
        else {
            proficiency.save(function (err) {
                if (err) { return next(err); }
                res.json({ title: 'Create Proficiency', save: true , proficiency: proficiency, errors: errors.array()});
            });
        }
    }
 ]

exports.proficiency_delete_get =function(req, res, next) {
   Proficiency.remove({_id:req.params.id},function(err) {
    if (err) { return next(err); }
        res.json( {delete :'done'} );
    });
};

exports.proficiency_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Proficiency Delete POST');
};

exports.proficiency_update_get = function(req, res) {
    Proficiency.findById(req.params.id )
          .exec( function(err, found_proficiency) {
                if (err) { return next(err); }
                if (found_proficiency) {
                    res.json( { title: 'Update Proficiency' ,update :'true', proficiency: found_proficiency,});
                }
            });
};

exports.proficiency_update_post =[
    body('title', 'Proficiency title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var proficiency = new Proficiency(
            {title: req.body.title,
            _id : req.params.id
        }
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json( { title: 'Update Proficiency' ,update :'true', proficiency: proficiency, errors: errors.array()});
        return;
        }
        else {
                Proficiency.findByIdAndUpdate(req.params.id, proficiency, {}, function (err,theproficiency) {
                    if (err) { console.log(err) }
                    res.json( {update :'done'} );
                });                        
            }
    }
 ]