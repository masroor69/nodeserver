var Disease = require('../models/disease');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.index = function(req, res) { 
    res.render('./disease/index');
};

exports.disease_list = function (req, res) {
      Disease
      .find()
      .exec(function (err, disease) {
        res.json( {
            disease: disease
        })
      })
};

exports.disease_detail = function(req, res, next) {
    
    Disease.findById(req.params.id)
    .exec(function (err, disease) {
      if (err) { return next(err) }
      if (disease==null) { 
          var err = new Error('Disease not found');
          err.status = 404;
        }
        res.json({ title: 'Disease:', disease:  disease});
    })
};

exports.disease_create_get = function(req, res) {
    res.json({ title: 'Create disease' });
};

exports.disease_create_post =[
    body('title', 'Disease title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
    sanitizeBody('disc').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var disease = new Disease(
          { title,
          disc }= req.body
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json({ title: 'Create Disease', disease: disease, errors: errors.array()});
        return;
        }
        else {
            disease.save(function (err) {
                if (err) { return next(err); }
                res.json({ title: 'Create Disease', save: true ,disease: disease, errors: errors.array()});
            });
        }
    }
 ]

exports.disease_delete_get =function(req, res, next) {
    // console.log(req.params.id);
   Disease.remove({_id:req.params.id},function(err) {
    if (err) { return next(err); }
        res.json( {delete :'done'} );
    });
};

exports.disease_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Disease delete POST');
};

exports.disease_update_get = function(req, res) {
    Disease.findById(req.params.id )
                .exec( function(err, found_disease) {
                     if (err) { return next(err); }
                     if (found_disease) {
                        res.json({ title: 'Update disease' ,update :'true', disease: found_disease,});
                     }
                    });
};

exports.disease_update_post =[
    body('title', 'Disease title required').isLength({ min: 1 }).trim(),
    sanitizeBody('title').trim().escape(),
    sanitizeBody('disc').trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);
        var disease = new Disease(
          { title: req.body.title,
            disc: req.body.disc,
            _id: req.params.id
        }
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json( { title: 'Update disease' ,update :'true', disease: disease, errors: errors.array()});
        return;
        }
        else {
                Disease.findByIdAndUpdate(req.params.id, disease, {}, function (err,thedisease) {
                    // if (err) { console.log(err) }
                    res.json( {update :'done'} );
                });                        
            }
    }
 ]  