var Doctor = require('../models/doctor');
var Proficiency = require('../models/proficiency');
var DoctorsProficiency = require('../models/doctorsProficiency')

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

exports.index = function(req, res) { 
    res.render('./doctor/index');
};

exports.doctor_list = function (req, res) {
      Doctor
      .find()
      .exec(function (err, doctor) {
            res.json({
                doctors: doctor
          })
     })
};

exports.doctor_detail = function(req, res, next) {
    async.parallel({
        experiences : function(callback){
              DoctorsProficiency.find({'doctor':req.params.id}).populate('proficiency')
              .exec(callback);
           },
        doctor: function(callback){
            Doctor.findById(req.params.id)
            .exec(callback)
           }
        },function (err, results) {
                if (err) { return next(err) }
                if (results==null) { 
                    var err = new Error('Doctor not found');
                    err.status = 404;
                    } 
                    res.json( { title: 'Doctor:', doctor:  results.doctor ,experiences: results.experiences});
                })
};

exports.doctor_create_get = function(req, res) {
    res.json( { title: 'Create Doctor' });
};

exports.doctor_create_post =[
        body('title', 'Doctor title required').isLength({ min: 1 }).trim(),
        body('name', 'Doctor Name required').isLength({ min: 1 }).trim(),
        body('age', 'Doctor Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        // console.log(req.body);
        var doctor = new Doctor(
            {title,
            name,
            age
        }=req.body
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json( { title: 'Create Doctor', doctor: doctor, errors: errors.array()});
        return;
        }
        else {
            doctor.save(function (err) {
                if (err) { return next(err); }
                res.json( { title: 'Create Doctor', save: true ,doctor: doctor, errors: errors.array()});
            });
        }
    }
 ]

exports.doctor_delete_get =function(req, res, next) {
   Doctor.remove({_id:req.params.id},function(err) {
    if (err) { return next(err); }
        res.json( {delete :'done'} );
    });
};

exports.doctor_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Doctor Delete POST');
};

exports.doctor_update_get = function(req, res) {
    async.parallel({
        experiences : function(callback){
            DoctorsProficiency.find({'doctor':req.params.id}).populate('proficiency')
            .exec(callback);
         },
         proficiencys : function(callback){
            Proficiency.find({})
            .exec(callback);
         },
        doctor: function(callback){
            Doctor.findById(req.params.id)
            .exec(callback)
        },
      },
       function (err, results) {
          if(err){return next(err);}
          // console.log(results)
          res.json( { title: 'Update Doctor' ,update :'true', doctor: results.doctor, proficiencys: results.proficiencys, experiences: results.experiences,});
      });
};

exports.doctor_update_post =[
    body('title', 'Doctor title required').isLength({ min: 1 }).trim(),
        body('name', 'Doctor Name required').isLength({ min: 1 }).trim(),
        body('age', 'Doctor Age Invalid between 0 - 99 ').isLength({ min: 1 , max:2 }).trim(),
        sanitizeBody('name').trim().escape(),
        sanitizeBody('age').trim().escape(),
        (req, res, next) => {
        const errors = validationResult(req);
        var doctor = new Doctor(
            {title: req.body.title,
            name: req.body.name,
            age: req.body.age,
            _id : req.params.id
        }
        );
        // console.log(req.body);
        if (!errors.isEmpty()) {
            res.json( { title: 'Update Doctor' ,update :'true', doctor: doctor, errors: errors.array()});
        return;
        }
        else {
                Doctor.findByIdAndUpdate(req.params.id, doctor, {}, function (err,thedoctor) {
                    if (err) { console.log(err) }
                    res.json( {update :'done'} );
                });                        
            }
    }
 ]